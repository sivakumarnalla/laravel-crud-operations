<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@yield('title') | Project</title>
  <meta name="Description" content="" />
  <meta name="Keyword" Content="" />
  <meta name="author" content="" />
  <link rel="shortcut icon" href="" />
  @section('styles')
  @show
  <style>
    .hgh420 {
      height: 420px
    }
.error_msg{
    color:red;
}

    #header1 {
      overflow: hidden;
    }

    .suggestions li {
      color: #000;
      padding: 5px 8px;
      border-bottom: 1px dashed #aaa;
      cursor: pointer;
      text-align: center;
    }

    .sticky {
      position: fixed;
      top: 0;
      width: 100%;
      z-index: 999
    }

    .sticky+.content {
      padding-top: 60px;
    }

    .dropdown-menu>li>a:focus,
    .dropdown-menu>li>a:hover {
      background-color: transparent !important;
    }

    .show-on-hover:hover>ul.dropdown-menu {
      display: block;
    }

    .logins-adjj {
      position: absolute;
      left: 6px;
      color: #333333;
    }
   
  </style>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Bootstrap -->
  <link href="{{asset('user/css/bootstrap.min.css')}}" type="text/css" rel="stylesheet" />
  <link rel="shortcut icon" href="" size="32*32">
  <!--custom css-->
  <link href="{{asset('user/css/custom.css')}}" type="text/css" rel="stylesheet" />
  <!--menu-->
  <link href="{{asset('user/css/menu.css')}}" type="text/css" rel="stylesheet" />
  <!-- Icomoon Icon Fonts-->
  <link href="{{asset('user/css/font-awesome.min.css')}}" rel="stylesheet" />

  <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
  <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
  <!-- Loading Function Script Start -->
  <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<!-- Loading Function Script End -->
</head>

<body class="home_bg_fixed">
  <!--header start--->
  <section id="header">
    <div class="container-fluid no-padd sticky" id="header1">
   
      <div class="clearfix"></div>

      <div class="col-md-12 header-menu">
        <div class="container">
          <div class="col-md-2 col-sm-2 no-pad logo">
            <a href="">
              <img src="https://www.pinclipart.com/picdir/big/544-5448427_technology-logo-png-hd-clipart.png" alt="logo" class="img-responsive logo-size" style="width:50px;height:50px;" /></a></div>
          <div class="col-md-10 header-menulist">
            <div id='cssmenu' class="col-md-12 no-pad">
              <ul>
                @auth
                <li class="">
                    <a href="" >
                    <!-- Authentication -->
                    <form method="POST" action="{{ route('logout') }}">
                        @csrf

                        <x-dropdown-link :href="route('logout')" id="cssmenu-logout" style="margin-top: -14px;"
                                onclick="event.preventDefault();
                                            this.closest('form').submit();">
                            {{ __('Log Out') }}
                        </x-dropdown-link>
                    </form>
                  </a>
                </li>
                <li class="<?php echo ($__env->yieldContent('title') == 'Products') ? 'active' : ''; ?>"><a href="{{ route('product-list') }}">Products</a></li>
                <li class="<?php echo ($__env->yieldContent('title') == 'dashboard') ? 'active' : ''; ?>"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                @else
                <li class="<?php echo ($__env->yieldContent('title') == 'Login') ? 'active' : ''; ?>"><a href="{{ route('login') }}">Login</a></li>
                <li class="<?php echo ($__env->yieldContent('title') == 'Register') ? 'active' : ''; ?>"><a href="{{ route('register') }}">Register</a></li>
                @endauth

              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
  </section>
  <!--header end--->
  <div class="clearfix"></div>
  <div class="clearfix">&nbsp;</div>

  

  @yield('content')


  <div class="clearfix"></div>
  <div class="clearfix">&nbsp;</div>
  <!--footer Start-->
  <footer class="footer">
    <div class="container">
      <div class="col-md-12 footer-main">
        <div class="clearfix">&nbsp;</div>
        <div class="col-md-4 no-padd">
          <div class="col-md-12 footer-main-left">
            <h2>About Us</h2>
            <p></p>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="col-md-4">
          <div class="col-md-12 footer-main-left">
             <h2 class="text-center">Useful Links</h2>
            <ul class="col-md-5">
              <li><a href="">Home</a></li>
              <li><a href="">Login</a></li>

            </ul>
            <ul class="col-md-7">
              <li><a href="">Register</a></li>
              <li><a href="">Products</a></li>

            </ul> 
            <div class="clearfix"></div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="col-md-12 footer-main-left">
            <h2>Contact Us</h2>
            <ul>

              <p>Mobile&nbsp;:&nbsp;+91 7288983258 </p>
              <p>Email:tech.sivakumar9190@gmail.com</p>
            </ul>
          </div>
        </div>

      </div>

      <div class="col-md-12 subfooter">
        <div class="container">
          <div class="col-md-12 subfooter-main">
            <ul class="list-inline">
              <li class="col-md-6 text-left">2020 All &copy;&nbsp;Reserved&nbsp;</li>
              <li class="col-md-6 text-right">Designed &amp; Developed By :<a href="" target="_blank">siva kumar</a></li>
            </ul>
          </div>
        </div>
      </div>
  </footer>
  <!--footer end-->
  
  <script src="{{asset('user/js/jquery.min.js')}}"></script>
  <script src="{{asset('user/js/bootstrap.min.js')}}"></script>
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
  @section('script')
  @show
</body>

</html>
