@extends('layouts.app')

@section('title', 'Points')

@section('content')
<section style="background:#fff;">
     <div class="container">
        <div id="flash-message">
        </div>
<br/>
        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6 ml-auto">
                <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#addProduct"><i class="fa fa-plus" aria-hidden="true"></i> Add Product</button>
                <button class="btn btn-success btn-md" name="search" id="search" onclick="updateActivationStatus('1')"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;Active</button>
                <button class="btn btn-warning btn-md" onclick="updateActivationStatus('0')"><i class="fa fa-ban" aria-hidden="true" ></i>&nbsp;In Active</button>
                <button class="btn btn-danger btn-md" onclick="commonDelete()"><i class="fa fa-trash" aria-hidden="true" onclick=""></i>&nbsp;Delete</button>
            </div>
          </div>
<br/>
<br/>
<br/>
<!-- Product Modal -->
<div id="addProduct" class="modal fade" role="dialog" style="margin-top: 106px">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
        <form id="store_product" method="post" enctype="multipart/form-data">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Product</h4>
      </div>
      <div class="modal-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Name</label>
              <input type="text" name="name" class="form-control" id="name" placeholder="Enter name">
              <span class="error_msg" id="nameErr"></span>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Price</label>
              <input type="text" class="form-control" id="price" name="price" maxlength="6" placeholder="Enter Price">
              <span class="error_msg" id="priceErr"></span>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">UPC</label>
                <input type="number" class="form-control" id="upc" name="upc" placeholder="Enter UPC">
                <span class="error_msg" id="upcErr"></span>
              </div>

            <div class="form-group">
                <label for="exampleFormControlFile1">Image</label>
                <input type="file" class="form-control-file" id="image" name="image" accept=".png, .jpg, .jpeg">
                <span class="error_msg" id="imageErr"></span>
            </div>


      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Add</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </form>
    </div>

  </div>
</div>

<!-- Update Product Modal -->
<div id="editProduct" class="modal fade" role="dialog" style="margin-top: 106px">
    <div class="modal-dialog modal-lg">

      <!-- Modal content-->
      <div class="modal-content">
          <form id="update_product" method="post" enctype="multipart/form-data">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Update Product</h4>
        </div>
        <div class="modal-body">
              <div class="form-group">
                <label for="exampleInputEmail1">Name</label>
                <input type="text" name="name" class="form-control" id="nameEdit" placeholder="Enter name">
                <input type="hidden" name="id" class="form-control" id="update_id">
                <input type="hidden" name="_method" value="PUT">
                <span class="error_msg" id="nameErrEdit"></span>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Price</label>
                <input type="text" class="form-control" id="priceEdit" name="price" maxlength="6"  placeholder="Enter Price">
                <span class="error_msg" id="priceErrEdit"></span>
              </div>
              <div class="form-group">
                  <label for="exampleInputPassword1">UPC</label>
                  <input type="number" class="form-control" id="upcEdit" name="upc" placeholder="Enter UPC">
                  <span class="error_msg" id="upcErrEdit"></span>
                </div>

              <div class="form-group">
                  <label for="exampleFormControlFile1">Image</label>
                  <input type="file" class="form-control-file" id="imageEdit" name="image" accept=".png, .jpg, .jpeg">
                  <span class="error_msg" id="imageErrEdit"></span>

              </div>
              <div class="form-group" id="old_img">
              </div>


        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Update</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
      </div>

    </div>
  </div>

    <div class="row">
      <h1>Manage Products</h1>
        <table class="table log_his display serversideDatatableForProducts table-responsive" id="myTable">
            <thead>
            <tr>
            <th><input type="checkbox" class="inline-checkbox" name="multiAction" id="multiAction"/> S.No.</th>
            <th>Name</th>
            <th>Price</th>
            <th>Image</th>
            <th>Upc</th>
            <th>Status</th>
            <th>Actions</th>
            </tr>
            </thead>

        </table>
    </div>
</div>

</section>
@endsection
@section('script')
@parent
<script>
     $(document).ready(function () {
            var table = productDataTable();
            table.on('draw.dt', function () {
                var info = table.page.info();
            });
        });
    function productDataTable($rows = 10){
            return $('.serversideDatatableForProducts').DataTable({
                //dom: 'frt',
                serverSide: true,
                processing: true,
                responsive: true,
                "bDestroy": true,
                "pageLength": $rows,
                ajax:"{{ route('product.index') }}",
                "order": [], //disable default sort ordering

                drawCallback: function () {
                },
                "language": { },
                "columns": [
                    {data: "srno"},
                    {data: "name"},
                    {data: "price"},
                    {data: "image"},
                    {data: "upc"},
                    {data: "status"},
                    {data: "actions"}
                ],
                'columnDefs': [ {
                'targets': [0,6], /* column index */
                'orderable': false, /* true or false */
            }]
            });
    }


    $('#store_product').on('submit',function(e){
            e.preventDefault();
            str = true;
            $('#name,#price,#upc').css('border','');
            $('#nameErr,#priceErr,#upcErr').html('');

            var name  = $("#name").val();
            var price = $("#price").val();
            var upc   = $("#upc").val();
            var image = $("#image").val();
            if(name == ''){
                $('#name').css('border','1px solid red');
                $('#name').focus();
                $('#nameErr').html('<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Please enter name');
                str = false;
            }
            if(price == ''){
                $('#price').css('border','1px solid red');
                $('#price').focus();
                $('#priceErr').html('<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Please enter price');
                str = false;
            }
            if(upc == ''){
                $('#upc').css('border','1px solid red');
                $('#upc').focus();
                $('#upcErr').html('<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Please enter upc');
                str = false;
            }

            if(str){
                // Loader.show();
                    $.ajax({
                            headers : {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: "{{ route('product.store')}}",
                            data: new FormData(this),
                            dataType: 'json',
                            type: "POST",
                            contentType: false,
                            cache: false,
                            processData: false,
                            success: function (response) {
                                if (response.code == 201) {
                                    $('#addProduct').modal('hide');
                                    productDataTable();
                                    $('#store_product')[0].reset();
                                    showAlertMessage(response.message,'success');
                                }else{
                                    showAlertMessage(response.message,'danger');
                                }
                                // Loader.hide();
                            },
                            error: function (response, data) {
                                // Loader.hide();
                                $('#addProduct').modal('hide');
                                showAlertMessage(response.responseJSON.message,'danger');
                            }
                        });
            }
        });

        function showAlertMessage(message, status) {
            let messageHTML = $('<div>', {'class': 'alert alert-'+status}).html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+message);
            $('#flash-message').empty().html(messageHTML).slideDown('fast');
        }

    function editProduct(id){
                        var url = '{{ route("product.edit", ":id") }}';
                        url = url.replace(':id', id);
                        $.ajax({
                            url: url,
                            dataType: 'json',
                            type: "GET",
                            contentType: false,
                            cache: false,
                            processData: false,
                            success: function (response) {
                                // Loader.hide();
                                if (response.code == 200) {
                                    $('#editProduct').modal('show');
                                    $('#nameEdit').val(response.data.name);
                                    $('#priceEdit').val(response.data.price);
                                    $('#upcEdit').val(response.data.upc);
                                    $('#old_img').html('');
                                    if(response.data.image_url !='' ){
                                    $('#old_img').html('<img style="width:200px;height:200px;" src="'+response.data.image_url+'" />');
                                    }
                                    $('#update_id').val(response.data.id);
                                }else{
                                        showAlertMessage(response.message,'danger');
                                    }
                            },
                            error: function (response, data) {
                                showAlertMessage(response.responseJSON.message,'danger');
                                // Loader.hide();
                            }
                        });
        }

        $('#update_product').on('submit',function(e){
            var url = '{{ route("product.update", ":id") }}';
            var update_id  = $("#update_id").val();
            url = url.replace(':id', update_id);
            e.preventDefault();
            str = true;
            $('#nameEdit,#priceEdit,#upcEdit').css('border','');
            $('#nameErrEdit,#priceErrEdit,#upcErrEdit').html('');

            var name  = $("#nameEdit").val();
            var price = $("#priceEdit").val();
            var upc   = $("#upcEdit").val();
            var image = $("#imageEdit").val();
            if(name == ''){
                $('#nameEdit').css('border','1px solid red');
                $('#nameEdit').focus();
                $('#nameErrEdit').html('<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Please enter name');
                str = false;
            }
            if(price == ''){
                $('#priceEdit').css('border','1px solid red');
                $('#priceEdit').focus();
                $('#priceErrEdit').html('<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Please enter price');
                str = false;
            }
            if(upc == ''){
                $('#upcEdit').css('border','1px solid red');
                $('#upcEdit').focus();
                $('#upcErrEdit').html('<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Please enter upc');
                str = false;
            }

            if(str){
                // Loader.show();

                var data = new FormData(this);

                var form=$("#update_product");
                    $.ajax({
                            headers : {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: url,
                            data: data,
                            contentType: false,
                            cache: false,
                            processData: false,
                            dataType: 'json',
                            type: "POST",
                            success: function (response) {
                                if (response.code == 201) {
                                    $('#editProduct').modal('hide');
                                    productDataTable();
                                    showAlertMessage(response.message,'success');
                                    $('#update_product')[0].reset();
                                }else{
                                    showAlertMessage(response.message,'danger');
                                }
                                // Loader.hide();
                            },
                            error: function (response, data) {
                                // Loader.hide();
                                $('#editProduct').modal('hide');
                                showAlertMessage(response.responseJSON.message,'danger');

                            }
                        });
            }
        });


        function updateActivationStatus(s) {
                                    var listarray = new Array();
                                    $('input[name="multiple[]"]:checked').each(function () {
                                        listarray.push($(this).val());
                                    });
                                    var checklist = "" + listarray;
                                    if (!isNaN(s) && (s == '1' || s == '0' || s == '2') && checklist != '') {
                                        $('#fail').hide();
                                        $.ajax({
                                            headers : {
                                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                },
                                            dataType: 'JSON',
                                            type: 'POST',
                                            data: { 'updatelist': checklist, 'activity': s},
                                            url: '{{ route("product-status") }}',
                                            success: function (response) {
                                                if (response.code == 200) {
                                                    productDataTable();
                                                    showAlertMessage(response.data +" "+ response.message,'success');
                                                    $('#multiAction').prop('checked', false);
                                                }else{
                                                    showAlertMessage(response.message,'danger');
                                                }
                                            },
                                            error: function (er) {
                                                console.log(er);
                                            }
                                        });
                                    } else {
                                        showAlertMessage('*  Please Select','danger');
                                    }
                                }
            function commonDelete() {
                                    var listarray = new Array();
                                    $('input[name="multiple[]"]:checked').each(function () {
                                        listarray.push($(this).val());
                                    });
                                    var checklist = "" + listarray;
                                    if ( checklist != '') {
                                        $('#fail').hide();
                                        $.ajax({
                                            headers : {
                                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                },
                                            dataType: 'JSON',
                                            type: 'POST',
                                            data: { 'updatelist': checklist},
                                            url: '{{ route("product-delete") }}',
                                            success: function (response) {
                                                if (response.code == 200) {
                                                    productDataTable();
                                                    showAlertMessage(response.data +" "+response.message,'success');
                                                    $('#multiAction').prop('checked', false);
                                                }else{
                                                    showAlertMessage(response.message,'danger');
                                                }
                                            },
                                            error: function (er) {
                                                console.log(er);
                                            }
                                        });
                                    } else {
                                        showAlertMessage('*  Please Select','danger');
                                    }
                                }
                                $('#multiAction').click(function () {
                                    if ($('#multiAction').is(':checked')) {
                                        $('#multiAction').prop('checked', true);
                                        $('[name="multiple[]"]').prop('checked', true);
                                    } else {
                                        $('#multiAction').prop('checked', false);
                                        $('[name="multiple[]"]').prop('checked', false);
                                    }
                                });
</script>
@endsection
