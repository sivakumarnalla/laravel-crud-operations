@extends('layouts.app')
@section('title', 'Dashboard')
@section('styles')
@parent
<style>

@media screen and (max-width: 34em) {
      .row-offcanvas-left .sidebar-offcanvas {
        left: -45%;
      }
      .row-offcanvas-left.active {
        left: 45%;
        margin-left: -6px;
      }
      .sidebar-offcanvas {
        width: 45%;
      }
    }

    .card {
      overflow: hidden;
    }

    .card-block .rotate {
      z-index: 8;
      float: right;
      height: 100%;
    }

    .card-block .rotate i {
      color: rgba(20, 20, 20, 0.15);
      position: absolute;
      left: 0;
      left: auto;
      right: 20px;
      bottom: 29px;
      display: block;
      -webkit-transform: rotate(-44deg);
      -moz-transform: rotate(-44deg);
      -o-transform: rotate(-44deg);
      -ms-transform: rotate(-44deg);
      transform: rotate(-44deg);
    }
</style>
@endsection
@section('content')

<section>
<div class="container">
  <div class="row">
    <div class="container-fluid">
        <div class="container">
          <div class="col-sm-12 bg-wh shadow" style="padding: 103px;">
            <div class="col-sm-8">
                <div class="row mb-3">
                    <div class="col-xl-4 col-lg-6">
                      <div class="card card-inverse card-danger">
                        <div class="card-block bg-danger" style="padding: 10px">
                          <div class="rotate">
                            <i class="fa fa-list fa-3x"></i>
                          </div>
                          <h6 class="text-uppercase">Products</h6>
                          <a href="{{ route('product-list') }}">View</a>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>
</section>
@endsection
