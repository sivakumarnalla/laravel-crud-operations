# Laravel 8.X, Mysql  and Bootstrap

Laravel Manage Products [CRUD Operations], Register,Login, Forgot password with Basic Authentication application built on Laravel 8.X, Mysql, and Bootstrap

| Laravel Products [CRUD Operations] |
| :------------ |
| Built on Laravel 8.x |
| Built on Bootstrap |
| Makes use of MySQL Database |
| Uses Laravel Repositories |
| Uses Laravel Middleware |
| Uses jQuery|

### I. Quick Project Setup - Installation

###### 1. Clone the repository using the following command in terminal:

	`sudo git clone https://gitlab.com/sivakumarnalla/laravel-crud-operations.git`

###### 2. Pull and Install Laravel Dependencies:

	`sudo composer update`

###### 3. Generate Key:

	`php artisan key:generate`

###### 4. Create your MySQL Database.
 
    Database name : siva  

###### 5. Copy the example file  using the following command in terminal from the projects root folder:

	`sudo cp .env.example .env`

###### 6. Configure the Database settings in the ```.env``` file.


###### 7. Setup Database Schema using the following command in terminal from the projects root folder:

	`sudo php artisan migrate` 
    

###### 8. Run below command to run project:

	`php artisan serve --port=8888`

###### 9. Open below link in broswer:

  Laravel development server started: http://127.0.0.1:8888
  
### II. Middlewares
* [XSS] -> Middleware (This middleware used for Preventing  Script Tag injections in Forms)
* [BasicAuth] -> Middleware (This middleware used for Basic Authentication)

### III. Basic Authentication

User Name : siva

Password : siva

###### Sample Screens:
1. Login: https://gitlab.com/sivakumarnalla/laravel-crud-operations/-/blob/main/public/demo_screens/login.PNG

2. Register: https://gitlab.com/sivakumarnalla/laravel-crud-operations/-/blob/main/public/demo_screens/register.PNG

3. Forgot Password: https://gitlab.com/sivakumarnalla/laravel-crud-operations/-/blob/main/public/demo_screens/forgotpwd.PNG

4. Manage Products: https://gitlab.com/sivakumarnalla/laravel-crud-operations/-/blob/main/public/demo_screens/manage_products.PNG

4. Email: https://gitlab.com/sivakumarnalla/laravel-crud-operations/-/blob/main/public/demo_screens/email.PNG

