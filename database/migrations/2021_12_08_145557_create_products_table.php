<?php

use App\Models\Product;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * @category	Migration
 * @package		Product
 * @author		Siva Kumar
 * @license
 * @link
 * @created_on	09-12-2021
 */
class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_id');
            $table->string('name', 30);
            $table->float('price');
            $table->string('image', 200)->nullable();
            $table->boolean('status')
                ->default(Product::STATUS_ENABLE);
            $table->string('upc',10)->nullable();
            $table->timestamp('created_at')
            ->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')
            ->default(
                DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')
            );
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
