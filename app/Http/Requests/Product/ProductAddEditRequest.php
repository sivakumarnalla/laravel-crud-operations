<?php

namespace App\Http\Requests\Product;

use App\Traits\FormatClientErrors;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @category	Request
 * @package		Product
 * @author		Siva Kumar
 * @license
 * @link
 * @created_on	09-12-2021
 *
 */
class ProductAddEditRequest extends FormRequest
{
    use FormatClientErrors;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          =>  'required|unique:products,name,'.$this->id.'|min:2|max:100',
            'price'         =>  'required|regex:/^\d+(\.\d{1,2})?$/',
            'upc'           =>  'required',
            'image'         =>  'sometimes|image|mimes:jpeg,bmp,png,jpg|max:4024',
        ];
    }

    /**
     * {@inheritDoc}
     * @see \Illuminate\Foundation\Http\FormRequest::messages()
     */
    public function messages()
    {
        return [
            'name.unique'    =>  $this->name.' already exists'
        ];
    }
}
