<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\Product\ProductAddEditRequest;
use App\Models\Product;
use App\Traits\ImageUpload;
use App\Traits\RestfulTrait;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

/**
 * @category	Controller
 * @package		Product
 * @author		Siva Kumar
 * @license
 * @link
 * @created_on	09-12-2021
 */
class ProductController extends Controller
{

    use RestfulTrait,ImageUpload;

    /**
     * Display a listing of the products.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {

        $user_id = auth()->user()->id;

        $products= Product::byUser($user_id)->get();
        return DataTables::of($products)
            ->addColumn('srno', function ($products) {
                return '<input type="checkbox" class="inline-checkbox" name="multiple[]" value="' .$products->id.  '"/>';
            })
            ->editColumn('name', function ($products) {
                return $products->name;
            })
            ->editColumn('price', function ($products) {
                return $products->price;
            })
            ->editColumn('image', function ($products) {
                if(!empty($products->image)){
                    return   '<img  alt="logo" style="width:50px;height:50px" src="' .$products->image_url.  '" alt="">';
                }else{
                    return   'NA';
                }
            })
            ->editColumn('upc', function ($products) {
                return $products->upc;
            })
            ->editColumn('status', function ($products) {
                if ($products->status == 1) {
                    return   '<p style="color:green">' . "Active" . '</p>';
                } else {
                    return '<p style="color:red">' . "In-Active" . '</p>';
                }
            })
            ->addColumn('actions', function ($products) {
                return '<a href="javascript:void(0)"</a><div class="action-drp-dwn action-btns"><button id="reg-user_ ' . $products->id . '"
                                         class="btn btn-success btn-md" onclick="editProduct(' . $products->id . ')">
                                         <i class="la la-ellipsis-v"></i>Edit
                                        </button>
                                            </div>';
            })
            ->rawColumns(['srno','image','status','actions'])
            ->make(true);
    }

    public function showProduct()
    {
        return view(
            'products.view'
        );
    }


    /**
     * Store a newly created product data.
     *
     * @param  App\Http\Requests\Product\ProductAddEditRequest;
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ProductAddEditRequest $request)
    {
        $data       =   $request->all();
        $user_id    =   auth()->user()->id;
        $product    =   new Product();
        if (
            $request->hasFile('image')
            && $request->file('image')->isValid()
        ) {
            $fileName = date("Ymd_His") . rand(0000, 9999) . '_'.$user_id;
            $data['image'] = $this->imageUpload(
                $request->file('image'), env('PRODUCT_IMG_PATH'),$fileName
            );
        }
        $data['user_id']    =   $user_id;
        $product->fill($data);
        $product->save();
        return $this->createdResponse(
            $product, trans('product.created')
        );
    }


    /**
     * Store a newly created product data.
     *
     * @param  App\Http\Requests\Product\ProductAddEditRequest;
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        $product    =   $this->checkResource($id);
        $user_id    =   auth()->user()->id;

        if (!$product instanceof Product) {
            return $product;
        }
        return $this->showSuccessResponse($product);
    }

    /**
     * Update a product
     *
     * @param App\Http\Requests\Product\ProductAddEditRequest $request
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ProductAddEditRequest $request, $id)
    {
        $data       =   $request->all();
        $product    =   $this->checkResource($id);
        $user_id    =   auth()->user()->id;
        if (!$product instanceof Product) {
            return $product;
        }
        if (
            $request->hasFile('image')
            && $request->file('image')->isValid()
        ) {
            $fileName = date("Ymd_His") . rand(0000, 9999) . '_'.$user_id;
            $data['image'] = $this->imageUpload(
                $request->file('image'), '/uploads/product/',$fileName
            );
            if(!empty($product->image)){
                unlink(public_path().'/uploads/product/'.$product->image);
            }
        }
        $product->fill($data);
        $product->save();
        return $this->createdResponse(
            $product, trans('product.updated')
        );
    }

    

    /**
     * Enable or Disable the Product
     *
     * @param  \Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function enableOrDisableProduct(
        \Request $request
    ) {
        $data= \Request::all();
        $update_ids = $data['updatelist'];
        $status     = $data['activity'];
        $user_id    =   auth()->user()->id;
        $product = Product::where('user_id',$user_id)->whereIn('id', explode(",",$update_ids))->update(['status' => $status]);
        
        return $this->showSuccessResponse(
            $product, trans('product.status')
        );
    }


    /**
     * Enable or Disable the Product
     *
     * @param  \Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroyProduct(
        \Request $request
    ) {
        $data= \Request::all();
        $update_ids = $data['updatelist'];
        $user_id    =   auth()->user()->id;
        $product = Product::where('user_id',$user_id)->whereIn('id', explode(",",$update_ids))->delete();
        
        return $this->showSuccessResponse(
            $product, trans('product.delete')
        );
    }

    /**
     * Check for the product exists
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse|\app\Models\Product
     */
    public function checkResource($id)
    {
        $product = Product::find($id);
        if (!$product) {
            return $this->notFoundResponse(
                trans('product.not_found')
            );
        }
        return $product;
    }

}