<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @category	Model
 * @package		Product
 * @author		Siva Kumar
 * @license
 * @link
 * @created_on	09-12-2021
 */
class Product extends Model
{
    use HasFactory;
    use SoftDeletes;

    const STATUS_ENABLE     =   1;
    const STATUS_DISABLE    =   0;
    protected $fillable = [
        'name', 'user_id', 'price', 'image', 'upc', 'status'
    ];
    protected $appends  =   ['image_url'];
    /*
     * Relations
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

     /*
     * Scopes
     */
    public static function scopeByUser($query,$user)
    {
        return $query->where('user_id', $user);
    }

    /*
     * Attributes
     */
    public function getImageUrlAttribute()
    {
        return $this->image ?
        url('uploads/product/'. $this->image) :
        "";
    }
}
