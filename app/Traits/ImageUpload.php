<?php
namespace App\Traits;
use Intervention\Image\Facades\Image;

trait ImageUpload
{
    protected function imageUpload($file, $destinationFloder, $fileName, $sizes = null,$folderReq=null) {
        /* image save in path */
        $image = Image::make(file_get_contents($file));
        $mime = Image::make(file_get_contents($file))->mime();
        $ext = explode('/', $mime);
        $extension = $ext[1];
        $floderPath = public_path().$destinationFloder;
        
        $returnImgRes = array();
        try {
            if ($sizes != null) {
                foreach ($sizes as $size) {
                    $width = $size['width'];
                    $heigth = $size['height'];
                    if($folderReq == null){
                    $createFloder = $width . "X" . "$heigth/"; /* if floder not exits create new otherwise skip */
                    $checkPath = $floderPath . $createFloder;
                    if (!file_exists($checkPath)) {
                        mkdir($checkPath, 0777, true);
                    }
                    $destinationPathThumb = $checkPath . $fileName . '.' . $extension;
                    }else{
                      $destinationPathThumb = $floderPath . $fileName . '.' . $extension;
                    }
                    array_push($returnImgRes, array('name' => $destinationPathThumb));
                    Image::make(file_get_contents($file))->resize($width, $heigth)->save($destinationPathThumb);
                }
            } else {

                $destinationPathThumb = $floderPath . $fileName . '.' . $extension;
                $resizePercentage = 70;
                Image::make(file_get_contents($file))->save($destinationPathThumb);
            }
            return $fileName . '.' . $extension;
        } catch (\Exception $e) {
            return false;
        }

        /* image save in path */
    }

}

?>
