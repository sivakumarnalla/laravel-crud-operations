<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::get('/product-list','App\Http\Controllers\Product\ProductController@showProduct')->middleware(['auth'])->name('product-list');
Route::post('/product-status','App\Http\Controllers\Product\ProductController@enableOrDisableProduct')->middleware(['auth'])->name('product-status');
Route::post('/product-delete','App\Http\Controllers\Product\ProductController@destroyProduct')->middleware(['auth'])->name('product-delete');

Route::resource(
    'product', 'App\Http\Controllers\Product\ProductController'
)->except(['create', 'show']);

require __DIR__.'/auth.php';
